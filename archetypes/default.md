---
title: "{{ replace .Name "-" " " | title }}"
subtitle: ""
date: {{ .Date }}
lastmod: {{ .Date }}
draft: true
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: ""

tags: []
categories: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-{{ .Name }}.jpg"
featuredImagePreview: "armannurhidayat-blog-{{ .Name }}.jpg"

toc:
  enable: true
math:
  enable: false
lightgallery: true
license: ""
---

{{ replace .Name "-" " " | title }}

<!--more-->
