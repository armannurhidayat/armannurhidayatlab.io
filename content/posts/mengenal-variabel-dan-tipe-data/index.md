---
title: "Belajar Python Dasar : Mengenal Variabel Dan Tipe Data"
subtitle: ""
date: 2021-10-13T22:23:26+07:00
lastmod: 2021-10-13T22:23:26+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Mengenal variabel dan tipe data pada python"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-mengenal-variabel-dan-tipe-data.jpg"
featuredImagePreview: "armannurhidayat-blog-mengenal-variabel-dan-tipe-data.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---
Variabel dan tipe data merupakan kedua hal yang tidak bisa dilepas ketika kita belajar bahasa pemrograman apapun. Pada kesempatan kali ini saya akan membahas tentang variabel dan tipe data yang ada pada Python.

### Variabel

Variabel merupakan lokasi di memori yang digunakan untuk menyimpan nilai sementara. Ketika kita membuat sebuah variabel artinya kita memesan beberapa tempat di dalam memori. Tempat tersebut bisa kita isi dengan data bilangan bulat (integer), pecahan (float), karakter (string), dan masih banyak lagi lainnya.

Di Python, kita bisa langsung memasukkan tipe data apa saja ke dalam variabel tanpa harus mendefinisikan tipe datanya terlebih dahulu. Karena ada beberapa bahasa pemrograman yang perlu mendefinisikan tipe data ketika membuat variabel seperti di Java dan Pascal.

Adapun aturan penamaan variabel di Python seperti:
1. Variabel tidak bisa diawali dengan angka.
2. Variabel tidak bisa menggunakan kata kunci Python seperti `def`, `print`, `if`, dll.
3. Variabel bisa menggunakan kombinasi huruf (a-z, A-Z), angka (0-9) dan garis bawah `_`.
4. Variabel bersifat _case-sensitive_, contoh variabel `Python` dengan `python` akan dianggap berbeda.

Berikut contoh lengkap penulisan variabel:

```python
nama = "Budi"
umur = 20
menikah = True

print('Nama:', nama)
print('Umur:', umur)
print('Status menikah:', menikah)
```

Penjelasan:

Dari contoh script Python di atas memiliki 3 variabel yaitu `nama`, `umur` dan `menikah`.
* Variabel `nama` memiliki nilai `Budi`.
* Variabel `umur` memiliki nilai `20`.
* Variabel `menikah` memiliki nilai `True`.


Sebagai contoh lain untuk penulisan variabel yang benar dan salah:
* _alamat :white_check_mark:
* 4lamat :x:
* alamat_lengkap :white_check_mark:
* alamat-lengkap :x:
* alamatLengkap :white_check_mark:

### Tipe Data

Tipe data adalah nilai dari variabel. Untuk cara penulisan tergantung dari jenis datanya, misal untuk tipe data dictionary di bungkus dengan kurung kurawal `{...}` dan tipe data string di bungkus dengan tanda kutip `"..."` sedangkan untuk tipe data boolean, integer dan float tidak perlu pembungkus.

Berikut adalah tipe data di bahasa pemrograman Python:

| Tipe Data        | Contoh                                   | Penjelasan            
|:---------------- |:---------------------------------------- |:------------------------------------------------------------------------------------ 
| Boolean          | `True` atau `False`                      | Menyatakan nilai logika `True` atau `False`
| String           | `"Python Dasar"`                         | Menyatakan karakter/kalimat bisa berupa huruf angka, dll (diapit tanda `"` atau `'`) 
| Integer          | `25` atau `1209` 	                      | Menyatakan bilangan bulat 
| Float            | `3.14` atau `0.99`                       | Menyatakan bilangan yang mempunyai koma 
| Hexadecimal      | `9a` atau `1d3`                          | Menyatakan bilangan dalam format heksa (bilangan berbasis 16) 
| Complex          | `1 + 5j`                                 | Menyatakan pasangan angka real dan imajiner 
| List             | `['abc', 99, 1.1]`                       | Data untaian yang menyimpan berbagai tipe data dan isinya bisa diubah-ubah 
| Tuple            | `('abc', 99, 1.1)`                       | Data untaian yang menyimpan berbagai tipe data tapi isinya tidak bisa diubah 
| Dictionary       | `{'nama':'arman nur hidayat', 'id':2}`   | Data untaian yang menyimpan berbagai tipe data berupa pasangan penunjuk dan nilai 

Contoh program sederhana menggunakan variabel dan tipe data:

```python
# Program check status vaksinasi
# membuat variabel dan memberikan nilai
nama = "Arman"
umur = 17
vaksin = False

# menampilkan isi variabel
print('Nama:', nama)
print('Umur:', umur)
print('Status vaksinasi:', vaksin)

# pengecekan dengan logika IF
if vaksin:
    print(nama + " sudah di vaksin")
else:
    print(nama + " belum di vaksin")
```

Kita juga bisa memeriksa atau mengetahui tipe data dari suatu variabel menggunakan fungsi `type()` dari bawaan Python.

Contoh penggunaanya seperti berikut:

```python
nilai = 17

print(type(nilai))
```

Jika menjalankan script di atas akan menampilkan output seperti ini `<class 'int'>`, artinya tipe data dari variabel `nilai` adalah integer.

### Konversi Tipe Data
Konversi tipe data merupakan cara untuk merubah tipe data dari nilai awal yang tersimpan dalam variabel. Salah satu tujuan kita perlu untuk konversi tipe data supaya data yang di input oleh user benar-benar valid dan bisa diproses oleh Python.

Contoh kasus dengan program berikut:

```python
# Program ramal umur
import datetime
thn_sekarang = datetime.datetime.now().year
thn_lahir = input('Masukkan tahun lahir: ')

print("Umur kamu sekarang", thn_sekarang - thn_lahir)
```

Jika di jalankan program di atas akan menampilkan error `TypeError: unsupported operand type(s) for -: 'int' and 'str'`, hal ini terjadi karena tipe data yang di input oleh user dianggap sebagai string. Kamu bisa cek tipe data dari variabel `thn_lahir` menggunakan function `type()` dan untuk menggunakan operator aritmatika subtraction `-` operan harus dengan tipe data number.

Untuk memperbaiki script dari contoh program di atas nilai yang di input user bisa diubah ke tipe data integer sebelum di proses oleh script selanjutnya. Dengan menggunakan fungsi `int()` menjadi:

```python
# Program ramal umur
import datetime
thn_sekarang = datetime.datetime.now().year
thn_lahir = input('Masukkan tahun lahir: ')

# proses konversi
thn_lahir = int(thn_lahir)

print("Umur kamu sekarang", thn_sekarang - thn_lahir)
```

Fungsi-fungsi Python lainnya untuk mengubah tipe data:

| Fungsi           | Penjelasan                                   
|:---------------- |:-------------------------------
| `int()`          | untuk mengubah menjadi integer
| `long()`         | untuk mengubah menjadi integer panjang
| `float()`        | untuk mengubah menjadi float
| `bool()`         | untuk mengubah menjadi boolean
| `chr()`          | untuk mengubah menjadi karakter
| `str()`          | untuk mengubah menjadi string
| `bin()`          | untuk mengubah menjadi bilangan Biner
| `hex()`          | untuk mengubah menjadi bilangan Heksadesimal
| `oct()`          | untuk mengubah menjadi bilangan Oktal
