---
title: "Synchronize Database Odoo Dengan OdooRPC"
subtitle: ""
date: 2021-10-07T11:25:41+07:00
lastmod: 2021-10-07T11:25:41+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Integrasi Odoo menggunakan OdooRPC"

tags: ["Programming", "Odoo", "Python"]
categories: ["Odoo"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-synchronize-database-odoo-dengan-odoorpc.jpg"
featuredImagePreview: "armannurhidayat-blog-synchronize-database-odoo-dengan-odoorpc.jpg"

toc:
  enable: true
math:
  enable: false
lightgallery: true
license: ""
---

__OdooRPC__ merupakan package Python yang dapat memudahkan developer untuk menguji coba server Odoo melalui RPC atau _Remote Procedure Call_.

<!--more-->

## 1 Requirements
OdooRPC support dari Odoo versi 0.8 ke atas. Sedangkan untuk Python support dari versi 2.7, 3.6, 3.7 dan 3.8.

## 2 Installation
Kamu bisa menginstall OdooRPC dengan cara copy command di bawah dan pastekan ke terminal:

```bash
pip install odoorpc
```

## 3 Tutorials
{{< admonition note >}}
Untuk keperluan tutorial kali ini saya menggunakan OdooRPC versi 0.8.0, akan diuji coba di Odoo versi 12.0, dengan
Python versi 3.8.2 dan menggunakan sistem operasi MacOS.
{{< /admonition >}}

### Create Database
Untuk berkomunikasi dengan Odoo server, kamu bisa menggunakan instance class `odoorpc.ODOO` seperti berikut ini:

```python
import odoorpc
odoo = odoorpc.ODOO('localhost', 'jsonrpc', 8069)
```

Penjelasan:
* __localhost__: host Odoo server.
* __jsonrpc__: protocol tersedia `jsonrpc` (default) dan `jsonrpc+ssl`.
* __8069__: port Odoo server.

```python
odoo.db.create('super_admin_passwd', 'apps', True, 'en_US', 'my_admin_passwd')
```

Penjelasan:
* __super_admin_passwd__: super admin password Odoo server.
* __apps__: nama database.
* __True__: demo data `True` atau `False`.
* __en_US__: bahasa yang ingin digunakan.
* __my_admin_passwd__: password database.

Untuk memeriksa database apa saja yang tersedia, bisa menggunakan property `odoo.db` dengan method `list()`:

```python    
odoo.db.list()
# ['apps']
```

{{< admonition note >}}
Untuk melakukan ini sebelumnya kamu perlu mengetahui _super admin password_ Odoo server.
Dan username default database yang sudah berhasil di buat adalah `admin`.
{{< /admonition >}}

### Login Database
Sekarang saya akan mencoba login ke Odoo dan menampilkan beberapa informasi,
seperti nama user yang sedang login beserta nama perusahaannya.

Gunakan method `login` pilih database dan akun Odooo yang sebelumnya berhasil kita buat.

```python
odoo.login('apps', 'admin', 'password')
```

Penjelasan:
* __apps__: nama database.
* __admin__: username Odoo.
* __password__: password Odoo.

Untuk menampilkan nama user dan nama perusahaannya bisa dengan cara berikut:

```python
user = odoo.env.user
print(user.name) # nama user yang terhubung
print(user.company_id.name) # nama perusahaannya
```

### CRUD
Selain melakukan beberapa hal di atas, dengan OdooRPC ini kita juga bisa melakukan CRUD atau Create, Read, Update, dan Delete. Saya akan mencontohkan penggunaan method CRUD pada object `product.product`, dan pastikan module Inventory sudah terinstall di Odoo kamu.

```python
product = odoo.env['product.product']

# Create product
product.create({
  'name': 'Victoria Arduino Eagle One T3 3 Group',
  'list_price': 200000000,
  'type': 'product',
  'description': 'Victoria Arduino Machine',
})

# Read product
product.search([])

# Update product
product.write([1], {
  'list_price': 2130000000,
})

# Delete product
product.unlink([1])
```

Untuk referensi dan mengetahui support fitur apa saja pada OdooRPC, bisa kunjungi website dokumentesinya di https://pythonhosted.org/OdooRPC.
