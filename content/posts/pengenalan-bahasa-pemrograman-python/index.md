---
title: "Belajar Python Dasar : Pengenalan Bahasa Pemrograman Python"
subtitle: ""
date: 2021-10-09T19:10:25+07:00
lastmod: 2021-10-09T19:10:25+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Pengenalan bahasa pemrograman python dasar"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-pengenalan-bahasa-pemrograman-python.jpg"
featuredImagePreview: "armannurhidayat-blog-pengenalan-bahasa-pemrograman-python.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Menurut survei yang di lakukan oleh [stackoverflow](https://stackoverflow.com) pada tahun 2021 ini. Python berhasil masuk di urutan ketiga menjadi bahasa pemrograman yang paling banyak digunakan oleh _developer_. Menggantikan posisi SQL yang pada tahun sebelumnya menempati posisi ketiga. Berikut persentase data hasil survei yang dilakukan lebih dari delapan puluh tiga ribu respondents:

![Most popular technologies](stackoverflow-technology-most-popular-technologies.jpg "sumber: https://insights.stackoverflow.com/survey/2021#technology-most-popular-technologies")

Selain itu Python juga menjadi urutan pertama untuk bahasa pemrograman yang paling banyak di cari oleh perusahaan.

![Most loved, dreaded, and wanted](stackoverflow-technology-most-loved-dreaded-and-wanted.jpg "sumber: https://insights.stackoverflow.com/survey/2021#technology-most-loved-dreaded-and-wanted")

So, udah tahu kan kenapa kamu perlu banget buat belajar bahasa pemrograman Python ini? Saya sendiri termasuk orang yang mengenal Python ketika saya bekerja di dalam perusahaan. Awalnya saya berpikir bahasa pemrograman ini akan sulit untuk dipelajari dan akan jarang menemukan tutorialnya di internet, ternyata saya salah.

__[Python](https://www.python.org)__ sendiri merupakan bahasa pemrograman interpretatif multiguna. Tidak seperti bahasa pemrograman lain yang susah untuk dibaca dan dipahami, Python lebih menekankan pada keterbacaan kode agar lebih mudah untuk memahami sintaks. Hal ini yang membuat Python sangat mudah dipelajari baik untuk kamu yang baru pertama kali ingin mempelajari bahasa pemrograman maupun untuk yang sudah menguasai bahasa pemrograman lain.

Bahasa yang muncul pertama kali pada tahun 1991, dan dibuat oleh seorang bernama [Guido van Rossum](https://id.wikipedia.org/wiki/Guido_van_Rossum) ini. Juga bisa digunakan untuk _enterprise_. Dalam tingkatan bahasa pemrograman, Python termasuk _high level language_. Python menjadi salah satu bahasa pemrograman yang _multi-platform_ dapat digunakan untuk membangun aplikasi, baik itu berbasis desktop, mobile ataupun berbasis web. Python juga mendukung hampir semua sistem operasi, bahkan untuk sistem operasi Linux, hampir semua distronya sudah menyertakan Python di dalamnya.

```python
print("Hello, World!")
```

Dengan fungsi `print()` seperti kode di atas, kamu sudah bisa menampilkan apapun yang kamu inginkan. Dibagian akhir kode pun tidak perlu di akhiri dengan tanda _semicolon_ `;` membuat kode yang kita tulis jadi terlihat lebih bersih. Kamu sebagai _developer_ pun bisa lebih fokus mengembangkan sistem yang dibuat ketimbang sibuk mencari-cari _syntax error_ yang hanya lupa tanda _semicolon_ di akhir kode.