---
title: "Panduan Install Ubuntu Server 20.04 LTS"
subtitle: ""
date: 2021-10-18T17:24:15+07:00
lastmod: 2021-10-18T17:24:15+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Panduan cara install ubuntu server"

tags: []
categories: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-panduan-install-ubuntu-server-20.04-lts.jpg"
featuredImagePreview: "armannurhidayat-blog-panduan-install-ubuntu-server-20.04-lts.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Server merupakan sistem komputer yang memiliki layanan khusus berupa penyimpanan data. Komputer server diharuskan untuk menyala terus selama 24 jam dan harus selalu online, oleh karena itu diperlukan ruangan khusus untuk menyimpannya agar tetap bisa bekerja dengan baik. Karena fungsinya sebagai pusat data, komputer server biasanya menggunakan spesifikasi lebih dari komputer normal pada umunya.

Pada artikel kali ini saya akan memberikan panduan untuk cara instalasi Ubuntu server 20.04 LTS. LTS sendiri adalah kepanjangan dari _Long Term Service_, dengan kata lain kamu akan mendapat update selama 5 tahun penuh sebelum disarankan untuk melakukan upgrade secara gratis ke versi yang lebih tinggi.

### Requirement / Kebutuhan:
1. File installer bisa di download melalui link berikut [https://ubuntu.com/download/server](https://ubuntu.com/download/server).
2. Media instalasi seperti DVD atau USB Flashdisk.
3. Koneksi internet, berguna untuk mengupdate package-package yang diperlukan selama proses instalasi.

### Langkah Instalasi
Langkah pertama adalah memasukkan DVD atau USB Flashdisk yang sudah di burn ke dalam komputer. Jika menggunakan Virtual Machine, pilih file ISO yang sudah didownload melalui source for CD/DVD Drive di VMWare atau Virtualbox tanpa melakukan burning ke DVD terlebih dahulu.

Setelah melakukan semua langkah di atas, kamu akan dihadapkan dengan tampilan pilihan bahasa seperti gambar dibawah. Pilih bahasa English kemudian tekan enter untuk melanjutkan.

![Pilih Bahasa](langkah-instalasi-ubuntu-server-01.png "Pilih Bahasa")

Selanjutnya akan menampilkan pengaturan layout pada keyboard. Biarkan saja default, pilih Done untuk melanjutkan.

![Keyboard Configuration](langkah-instalasi-ubuntu-server-02.png "Keyboard Configuration")

Jika komputer kamu terhubung ke jaringan, maka di layar berikutnya akan menampilkan antarmuka (Ethernet Card) secara otomatis memilih IP melalui DHCP. Jika pengaturan sudah benar pilih Done untuk melanjutkan.

![Network Connection](langkah-instalasi-ubuntu-server-03.png "Network Connection")

Jika jaringanmu membutuhkan proxy untuk terhubung ke internet, kamu bisa mengaturnya disini. Jika tidak, bisa diabaikan dan langsung pilih Done.

![Proxy Configuration](langkah-instalasi-ubuntu-server-04.png "Proxy Configuration")

Di layar berikutnya, berdasarkan lokasi negara. Installer akan secara otomatis mengkonfigurasi mirror arsip Ubuntu. Mirror merupakan server yang digunakan oleh ubuntu untuk melakukan update dan install aplikasi. Jika ingin koneksi yang cepat dan stabil, saya sarankan untuk menggunakan mirror address dari server yang berlokasi di indonesia.

![Mirror Configuration](langkah-instalasi-ubuntu-server-05.png "Mirror Configuration")

Selanjutnya kamu bisa mengatur harddisk yang akan dilakukan instalasi dari file Ubuntu server. Agar lebih mudah pilih pengaturan default. Kemudian pilih Done.

![Storage Configuration](langkah-instalasi-ubuntu-server-06.png "Storage Configuration")

Setelah selesai dengan pengaturan partisi, Akan menampilkan halaman berikut, verifikasi partisi dan kemudian pilih Done.

![Storage Configuration](langkah-instalasi-ubuntu-server-07.png "Storage Configuration")

Pilih Continue untuk melanjutkan proses instalasi.

![Storage Configuration](langkah-instalasi-ubuntu-server-08.png "Storage Configuration")

Pada halaman ini, Kamu bisa menentukan nama, nama server, username dan password sesuai keinginan. Kemudian pilih Done.

![Profile Setup](langkah-instalasi-ubuntu-server-09.png "Profile Setup")

Jika kamu ingin mengakses server melalui jaringan menggunakan SSH maka disarankan untuk menginstal package server OpenSSH.

![SSH Setup](langkah-instalasi-ubuntu-server-10.png "SSH Setup")

Di halaman ini banyak package yang bisa sekalian kamu install, kamu bisa pilih salah satu kemudian pilih Done.

![Featured Server Snaps](langkah-instalasi-ubuntu-server-11.png "Featured Server Snaps")

Proses instalasi sedang berlangsung, tunggu beberapa menit.

![Proses Install](langkah-instalasi-ubuntu-server-12.png "Proses Install")

Setelah penginstalan selesai, selanjutnya akan meminta untuk me-reboot komputer.

![Proses Install](langkah-instalasi-ubuntu-server-13.png "Proses Install")

Setelah komputer selesai reboot, selanjutnya akan menampilkan halaman login Ubuntu, ini juga sebagai tanda jika semua proses instalasi sudah selesai. Kamu bisa melakukan login dengan user dan password yang sudah dibuat saat profile setup di prosess instalasi tadi.

![Halaman Login](langkah-instalasi-ubuntu-server-14.png "Halaman Login")

Berikut halaman Ubuntu server setelah berhasil melakukan login.

![Halaman Ubuntu Server](langkah-instalasi-ubuntu-server-15.png "Halaman Ubuntu Server")