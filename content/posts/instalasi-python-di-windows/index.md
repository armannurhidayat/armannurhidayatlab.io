---
title: "Belajar Python Dasar : Instalasi Python Di Windows"
subtitle: ""
date: 2021-10-10T11:36:21+07:00
lastmod: 2021-10-10T11:36:21+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "instalasi python di windows"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-instalasi-python-di-windows.jpg"
featuredImagePreview: "armannurhidayat-blog-instalasi-python-di-windows.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---


Seperti yang sudah pernah saya singgung di seri belajar Python dasar episode sebelumnya [Pengenalan Bahasa Pemrograman Python](/pengenalan-bahasa-pemrograman-python), bahwa Python mendukung hampir semua sistem operasi dan untuk kamu pengguna sistem operasi Linux atau MacOS sudah tidak perlu lagi melakukan proses instalasi karena sudah tersedia di dalam sistem operasinya, Terkecuali kalau kamu mau upgrade atau downgrade versi Python yang sudah tersedia dari sistem operasinya.

Maka dari itu untuk kamu pengguna dari sistem operasi Linux atau MacOS bisa melewati episode ini, karena disini saya hanya akan membahas proses instalasi Python pada sistem operasi Windows saja.

### Let’s install Python

Cara install Python sangatlah mudah, Pertama silahkan akses halaman resmi Python di [https://www.python.org/downloads/windows](https://www.python.org/downloads/windows) untuk mendownload file installer. Saat ini Python memiliki 2 versi yang berbeda, yaitu Python versi __3.10.0__ dan Python versi __2.7.18__. Saya sarankan untuk menggunakan Python versi terbaru __3.10.0__, versi Python ini juga yang akan saya gunakan untuk membahas seri belajar bahasa Python dasar kedepannya.

![Python release version](python-relase-version.png)

Setelah di download selanjutnya kita install dengan mengklik 2 kali (double click) pada file installer Python sehingga muncul tampilan seperti ini.

![Python installation](langkah-instalasi-python-01.png)

Jangan lupa untuk mencentang pada bagian _Add Python 3.10 to PATH_, kemudian pilih _Install Now_, nantinya akan muncul pop up konfirmasi seperti di bawah.

![Python installation](langkah-instalasi-python-02.png)

Bisa langsung pilih _Yes_ dan proses installasi akan berjalan. Tunggu beberapa menit sampai benar-benar selesai.

![Python installation](langkah-instalasi-python-03.png)

Ini halaman instalasi terakhir. Tekan tombol Close, untuk menutup halaman installer.

![Python installation](langkah-instalasi-python-04.png)

Selamat!, sampai sini proses instalasi Python sudah selesai, untuk memastikannya kamu bisa buka Command Prompt atau CMD kemudian ketikan perintah `python` untuk masuk ke Python shell.

![Python installation](langkah-instalasi-python-05.png)