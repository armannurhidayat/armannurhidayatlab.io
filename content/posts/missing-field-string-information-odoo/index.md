---
title: "Resolved : Missing Field String Information Odoo Versi 16.0"
subtitle: ""
date: 2024-10-29T18:17:04+07:00
lastmod: 2024-10-29T18:17:04+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Error Missing Field String Information di Odoo Versi 16.0"

tags: ["Odoo"]
categories: ["Odoo"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "missing-field-string-information-odoo.jpg"
featuredImagePreview: "missing-field-string-information-odoo.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

`Error: Missing field string information for the field 'cancel_ids' from the 'property.contract' model`

![Error Missing Field String Information Odoo 16.0](error-missing-field-string-information-odoo-16.png "Error Missing Field String Information Odoo 16.0")

Biasanya terjadi di Odoo (ERP) ketika ada field yang didefinisikan dalam model tetapi tidak memiliki parameter `string`, yang merupakan label atau nama tampilan untuk field tersebut. Parameter `string` sangat penting karena berfungsi untuk memberi label pada field di antarmuka pengguna.

<br/>

Berikut adalah langkah-langkah untuk memperbaiki masalah ini:

1. **Temukan Definisi Field yang Bermasalah**: Temukan definisi field `cancel_ids` dalam model `property.contract`. Biasanya, ini ada di file Python dalam direktori `models`.

2. **Tambahkan Parameter** `string`: Pastikan bahwa field `cancel_ids` didefinisikan dengan parameter `string`. Jika field ini belum memiliki parameter tersebut, tambahkan dengan label yang sesuai.

<br/>

Contoh perbaikan:

```python
from odoo import models, fields

class PropertyContract(models.Model):
    _name = 'property.contract'
    _description = 'Property Contract'

    cancel_ids = fields.Many2many('other.model', string="Cancellations")
```

3. **Restart Server dan Update Modul**: Setelah memperbaiki kode sesuai seperti contoh di atas, restart server Odoo kamu dan lakukan update pada modul `property.contract`. Ini akan memuat ulang model dengan definisi field terbaru.

<br/>

Jalankan perintah berikut untuk meng-update modul dari terminal:

```bash
./odoo-bin -c <config_file> -u <module_name>
```

4. **Cek Log untuk Error Tambahan**: Setelah update, periksa kembali log untuk memastikan bahwa pesan error sudah benar-benar hilang dan Odoo dapat berjalan dengan normal.
