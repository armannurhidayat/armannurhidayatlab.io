---
title: "Update Module Otomatis Tanpa Restart Server Odoo"
subtitle: ""
date: 2021-10-27T23:25:44+07:00
lastmod: 2021-10-27T23:25:44+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Update module otomatis tanpa restart server odoo"

tags: ["Odoo"]
categories: ["Odoo"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-update-module-otomatis-tanpa-restart-server-odoo.jpg"
featuredImagePreview: "armannurhidayat-blog-update-module-otomatis-tanpa-restart-server-odoo.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Selama proses development Odoo kita seringkali melakukan restart server melalui terminal Linux atau command prompt di Windows untuk update module. Memang ini menjadi hal yang mudah untuk dilakukan, tapi bagaimana kalau kamu berulang kali melakukan restart server hanya sekedar merubah logic pada file Python atau memperbaiki kalimat typo yang ada pada report di file XML yang kamu buat?

Tentunya ini akan memakan banyak waktu.

Ada cara untuk melakukan update module tanpa perlu restart server Odoo yaitu dengan menambahkan parameter `--dev={DEV_MODE}` ketika menjalankan server Odoo melalui terminal.

Langkah pertama yang perlu kamu lakukan adalah menginstall package Python, dengan perintah command dibawah ini.

```bash
pip3 install watchdog
```

Setelah menginstalnya, jalankan server Odoo dengan contoh command dibawah.

```bash
/odoo/odoo_v12/odoo-server/odoo-bin -c /etc/odoo12-server.conf --dev=reload
```

{{< admonition note >}}
PATH `odoo-bin` dan `odoo.conf` bisa di sesuaikan ketika menginstall Odoo.
{{< /admonition >}}

Opsi pada dev mode ada banyak macamnya seperti `[pudb|wdb|ipdb|pdb]`, `reload`, `qweb`, `xml`, `werkzeug` dan `all`. Kamu bisa menggunakan salah satu atau lebih dengan tanda comma sebagi pemisah `--dev=reload,xml`.

<br/>
Penjelasan:

* `[pudb|wdb|ipdb|pdb]` untuk mengaktifkan Python Debugger (PDB), kamu bisa menggunakan `--dev=pdb` untuk mengaktifkannya.
* `reload` dengan menggunakan `--dev=reload` akan restart Odoo secara otomatis, ketika kamu melakukan perubahan syntax pada Python dan menyimpannya.
* `qweb` ketika membuat template QWeb, kamu dapat mendebug menggunakan t-debug attribute dengan menggunakan `--dev=qweb`.
* `xml` dengan `--dev=xml` ketika kamu mengupdate view yang ada pada file xml dan menyimpannya, kamu cukup mereload browser tanpa perlu restart Odoo.
* `werkzeug` Odoo menggunakan werkzeug untuk meng-handle HTTP Request. jika menggunakan `--dev=werkzeug` maka pada browser akan muncul werkzeug debugger ketika exception tergenerate.
* `all` jika menggunakan `--dev=all` maka semua pilihan di atas akan aktif.
