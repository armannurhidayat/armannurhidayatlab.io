---
title: "Belajar Python Dasar : Aturan Penulisan Sintaks Python"
subtitle: ""
date: 2021-10-13T11:11:22+07:00
lastmod: 2021-10-13T11:11:22+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Aturan penulisan sintaks python"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-aturan-penulisan-sintaks-python.jpg"
featuredImagePreview: "armannurhidayat-blog-aturan-penulisan-sintaks-python.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Terdapat beberapa aturan dalam penulisan sintaks pada bahasa pemrograman Python. Aturan-aturan ini harus dipatuhi oleh kamu sebagai programmer. Jika tidak, maka aplikasi yang dibuat bisa terjadi error.

Pada artikel kali ini, saya akan membahas beberapa aturan dasar dalam penulisan sintaks Python yang perlu kamu ketahui di antaranya adalah:

### Penulisan Statement
Statemen adalah sebuah pernyataan atau instruksi yang akan dieksekusi oleh komputer. Di dalam Python, penulisan antar statemen tidak diakhiri dengan titik koma. Sedangkan cara interpreter Python membedakan antara satu statemen dengan statemen lainnya adalah dengan baris baru.

```python
blog = "armannurhidayat.com"
print(blog)
print("Tutorial Python Dasar")
```

Pada contoh program di atas, terdapat 3 buah statement.

Kita bisa saja menulis lebih dari satu statement dengan satu baris menggunakan titik koma seperti ini:

```python
blog = "armannurhidayat"; print(blog); print("Tutorial Python Dasar")
```

Tetapi cara di atas tidak saya sarankan, karena akan sulit untuk dibaca oleh programmer lain atau bahkan kita sendiri nantinya.

### Case Sensitive
Salah satu kesalahan yang sering dilakukan oleh programmer ketika menulis script Python adalah tidak memperhatikan huruf besar dan kecil pada variabel yang sudah dibuatnya. Bahasa pemrograman Python sendiri bersifat _case sensitive_. Dimana ketika menulis huruf besar dan kecil sangat berpengaruh.

```python
Title = "Case Sensitive Pada Python"
title = "Case Sensitive Pada Python"
```
Dua contoh penulisan variabel di atas antara `Title` dan `title` tidaklah sama.

### Penulisan String
String merupakan tipe data yang digunakan untuk menyimpan barisan karakter. Cara penulisan string pada Python bisa menggunakan _single quote_ `'`, _double quote_ `"`, atau _triple quote_ `'''`. Yang tidak bisa, jika di awal pembungkus karakter menggunakan _single quote_ kemudian di akhir pembungkus menggunakan _double quote_ atau sebaliknya.

```python
title = 'Belajar Python Dasar'
title = "Belajar Python Dasar"
title = """Belajar Python Dasar"""
title = '''Belajar Python Dasar'''
```

### Indentasi Pada Python
Indentasi adalah penulisan paragraf yang agak menjorok masuk ke dalam. Pada bahasa pemrograman lain indentasi ini tidak begitu berpengaruh, lain halnya di Python indentasi ini sangatlah penting. Karena ia lah yang bertugas untuk mendefinisikan struktur blok kode program.

Berikut contoh cara penulisan indentasi pada Python yang benar dan salah:

Contoh yang benar :white_check_mark::
```python
nilai = 8

if nilai > 9:
  print("Nilai lebih dari 9")
else:
  print("Nilai kurang dari 9")
```

Contoh yang salah :x::
```python
nilai = 8

if nilai > 9:
print("Nilai lebih dari 9")
else:
      print("Nilai kurang dari 9")
```

### Penulisan Komentar
Komentar adalah baris kode Python yang tidak akan dieksekusi atau akan diabaikan oleh interpreter Python. Komentar biasa digunakan sebagai penjelasan alur dari kode program yang kita tulis atau bisa juga untuk menonaktifkan kode. Penulisan komentar pada Python cukup dengan tanda pagar `#`.

```python
# contoh penulisan komentar di python
# tulisan ini tidak akan di eksekusi

kota = "Bandung"
print(kota) # cetak variabel kota
```

Saya kira sampai di sini dulu pembahasan mengenai aturan penulisan sintaks di Python. Pembahasan di atas merupakan hal-hal umum tentang cara menulis kode di Python. Dan insyaallah pada pertemuan selanjutnya kita akan mempelajari [Mengenal Variabel Dan Tipe Data Di Python](/mengenal-variabel-dan-tipe-data).

Sampai jumpa di artikel belajar python dasar selanjutnya... :wave: