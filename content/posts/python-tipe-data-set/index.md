---
title: "Belajar Python Dasar : Tipe Data Set"
subtitle: ""
date: 2021-10-20T20:15:33+07:00
lastmod: 2021-10-20T20:15:33+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Tipe data set di python"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-python-tipe-data-set.jpg"
featuredImagePreview: "armannurhidayat-blog-python-tipe-data-set.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Di Python sendiri terdapat 4 tipe data collection yang digunakan untuk menyimpan kumpulan data dalam satu variabel diantaranya yaitu [List](/python-tipe-data-list), [Tuple](/python-tipe-data-tuple), Set dan [Dictionary](/python-tipe-data-dictionary) tentunya dengan cara penggunaan yang berbeda-beda. Namun pada artikel kali ini saya akan fokus membahas tipe data Set.

Tipe data set berbeda dengan tipe data list dan tuple yang sudah pernah dibahas pada artikel sebelumnya. Tipe data set bersifat _unordered_ (output yang ditampilkan tidak berurutan) dan _unindexed_ (tidak bisa diakses dengan indeks). Tipe data set berbeda dengan tipe data lain, set tidak menampilkan nilai yang duplikat atau _unique value_.

### Membuat Set
Untuk membuat tipe data set nilai dibungkus dengan tanda kurung kurawal `{}` dan tiap nilai dipisahkan menggunakan tanda koma `,`. Nilai pada tipe data set bisa berisi berbagai tipe data seperti string, number dan boolean.

Contoh:
```python
myset = {'set', 232, 100, 0.2, False, True}
print(myset)

# tipe data set tidak menampilkan nilai duplikat
data = {1, 1, 2, 3, 4, 4}
print(data)
```

Akan tetapi untuk tipe data list, dictionary dan set itu sendiri, tidak bisa disimpan pada tipe data set.

Contoh:
```python
myset = {1, 4, 'set', [1, 2]}
print(myset)
```

jika dijalankan script di atas akan menampilkan error `TypeError: unhashable type: 'list'`.

### Menambahkan Nilai Set
Untuk menambahkan nilai pada set, bisa menggunakan fungsi yang sudah disediakan oleh Python yaitu `add()` dan fungsi `update()` untuk menambahkan lebih dari satu nilai sekaligus. Berikut untuk cara penggunaanya:

##### 1. `set.add(nilai)`
Fungsi `add()` membutuhkan satu parameter, yang dimana parameter tersebut adalah nilai yang akan ditambahkan kedalam set.

Contoh:
```python
warna = {'Putih', 'Hitam'}
print(warna)

# menambahkan nilai Merah pada set
warna.add('Merah')
print(warna)
```

Hasil output:

    {'Hitam', 'Putih'}
    {'Hitam', 'Putih', 'Merah'}

##### 2. `set.update([nilai])`
Sedangkan fungsi `update()` menggunakan list sebagai parameter untuk menambahkan lebih dari satu nilai baru ke dalam set.

Contoh:
```python
warna = {'Putih', 'Hitam'}
print(warna)

# menambahkan nilai Merah dan Biru pada set
warna.update(['Merah', 'Biru'])
print(warna)
```

Hasil output:

    {'Putih', 'Hitam'}
    {'Putih', 'Hitam', 'Merah', 'Biru'}

### Menghapus Nilai Set
Untuk menghapus nilai yang ada pada set, ada 4 fungsi yang bisa di gunakan `remove()`, `discard()`, `pop()` dan `clear()`.

##### 1. `set.remove(nilai)`
Fungsi `remove()` ini akan menghapus nilai sesuai dengan nilai yang dimasukan sebagai parameter. Jika nilai yang dimasukan tidak ada, maka akan menampilkan error.

Contoh:
```python
warna = {'Merah', 'Biru', 'Putih'}

# akan error jika nilai Putih tidak ada di dalam set
warna.remove('Putih')
print(warna)
```

Hasil output:
    
    {'Merah', 'Biru'}


##### 2. `set.discard(nilai)`
Kebalikannya dari fungsi `remove()` di atas fungsi `discard()` ini tidak akan menampilkan error jika nilai yang dimasukan sebagai paramternya tidak ada di dalam set.

Contoh:
```python
warna = {'Merah', 'Biru', 'Putih'}

# tidak akan error jika nilai Putih tidak ada di dalam set
warna.discard('Putih')
print(warna)
```

Hasil output:
    
    {'Merah', 'Biru'}

##### 3. `set.pop()`
Fungsi `pop()` ini akan menghapus nilai output yang ada di sebelah kiri pada set.

Contoh:
```python
warna = {'Merah', 'Biru', 'Putih'}

print('Menghapus nilai: ', warna.pop())
print(warna)
```

Hasil output:
    
    {'Putih', 'Biru'}

##### 4. `set.clear()`
Fungsi `clear()` ini akan menghapus semua nilai yang ada di dalam set.

Contoh:
```python
warna = {'Merah', 'Biru', 'Putih'}
warna.clear()
print(warna)
```

Hasil output:
    
    set()

### Operasi Pada Set
Selain kegunaan dan keunggulan yang dimiliki tipe data set di atas, selanjutnya saya akan contohkan beberapa operasi himpunan. Yang sebenarnya tipe data set ini digunakan untuk kebutuhan operasi himpunan, seperti operasi gabungan (union), irisan (intersection), selisih (difference) dan komplemen (symmetric difference).

##### 1. Gabungan (Union)
Kita bisa melakukan penggabungan atau operasi union, menggunakan set dengan simbol pipe `|` atau bisa juga menggunakan fungsi `union()`.

![Ilustrasi operasi union](ilustrasi-operasi-union.png "Ilustrasi operasi union")

Contoh:
```python
data1 = {0, 1, 2, 3, 4}
data2 = {5, 6, 7, 8, 9}

# dengan simbol pipe |
result1 = data1 | data2
print(result1)

# dengan fungsi union()
result2 = data1.union(data2)
print(result2)
```

Hasil output:
    
    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

##### 2. Irisan (Intersection)
Irisan atau operasi intersection, yaitu pengambilan anggota – anggota yang berada pada dua himpunan. Kita bisa menggunakan simbol ampersand `&` atau dengan fungsi pada Python `intersection()`.

![Ilustrasi operasi intersection](ilustrasi-operasi-intersection.png "Ilustrasi operasi intersection")

Contoh:
```python
data1 = {1, 2, 3, 4, 5}
data2 = {1, 9, 6, 2, 3}

# dengan simbol ampersand &
result1 = data1 & data2
print(result1)

# dengan fungsi intersection()
result2 = data1.intersection(data2)
print(result2)
```

Hasil output:
    
    {1, 2, 3}
    {1, 2, 3}

##### 3. Selisih (Difference)
Selisih atau operasi difference, yaitu pengambilan anggota dari himpunan pertama yang bukan anggota dari himpunan kedua. Untuk menggunakannya bisa menggunakan simbol minus `-` atau dengan fungsi `difference()`.

![Ilustrasi operasi difference](ilustrasi-operasi-difference.png "Ilustrasi operasi difference")

Contoh:
```python
data1 = {1, 2, 3, 4, 5}
data2 = {1, 9, 6, 2, 3}

# dengan simbol minus -
result1 = data1 - data2
print(result1)

# dengan fungsi difference()
result2 = data1.difference(data2)
print(result2)
```

Hasil output:

    {4, 5}
    {4, 5}

##### 4. Komplemen (Symmetric Difference)
Komplemen atau operasi symmetric difference, yaitu pengambilan anggota-anggota dari kedua himpunan, yang mana anggota tersebut hanya ada di satu himpunan saja. Untuk cara penggunaan bisa dengan simbol caret `^` atau dengan fungsi `symmetric_difference()`.

![Ilustrasi symmetric difference](ilustrasi-operasi-symmetric-difference.png "Ilustrasi symmetric difference")

Contoh:
```python
data1 = {1, 2, 3, 4, 5}
data2 = {1, 2, 3, 8, 9}

# dengan simbol caret ^
result1 = data1 ^ data2
print(result1)

# dengan fungsi symmetric_difference()
result2 = data1.symmetric_difference(data2)
print(result2)
```

Hasil output:

    {4, 5, 8, 9}
    {4, 5, 8, 9}

### Fungsi-fungsi Pada Set
Masih banyak fungsi-fungsi yang bisa digunakan pada tipe data set ini, seperti:

| Fungsi                           | Keterangan
|:-------------------------------- |:--------------------------------------------------------------------------
| `add()`                          | Menambahkan satu nilai ke dalam set
| `update()`                       | Menambahkan lebih dari satu nilai ke dalam set
| `clear()`                        | Menghapus semua nilai pada set
| `copy()`                         | Membuat salinan set
| `difference()`                   | Melakukan operasi selisih antar dua set
| `discard()`                      | Menghapus nilai yang ada pada set
| `remove()`                       | Menghapus nilai yang ada pada set
| `isdisjoint()`                   | Mengembalikan nilai `True` jika dua set tidak memiliki irisan
| `issusbset()`                    | Mengembalikan nilai `True` jika set lain memiliki anggota dari set sekarang
| `issuperset()`                   | Mengembalikan nilai `True` jika set sekarang memiliki anggota dari set lain
| `pop()`                          | Menghapus nilai pada set dan mengembalikan nilai
| `symmetric_difference()`         | Melakukan operasi komplemen antar dua set
| `symmetric_difference_update()`  | Mengupdate set dari hasil komplemen
| `union()`                        | Melakukan penggabungan antara dua set

{{< admonition note >}}
Jika dari semua contoh script di atas menampilkan output yang berbeda dari yang kamu jalankan, perlu diingat bahwa tipe data set yang bersifat _unordered_.
{{< /admonition >}}
