---
title: "Resolved : Perubahan Format Report Odoo Versi 14.0"
subtitle: ""
date: 2022-09-28T11:42:43+07:00
lastmod: 2022-09-28T11:42:43+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Perubahan penulisan format report di Odoo versi 14.0"

tags: ["Odoo"]
categories: ["Odoo"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-perubahan-format-report-odoo-14.jpg"
featuredImagePreview: "armannurhidayat-blog-perubahan-format-report-odoo-14.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Warning yang akan tampil di terminal log jika kamu menggunakan syntax Odoo versi 13.0 kebawah untuk membuat action report.

`The <report> tag is deprecated, use a <record> tag for 'penagihan_faktur'.
warnings.warn(f"The <report> tag is deprecated, use a <record> tag for {xml_id!r}.", DeprecationWarning)`

![Error Format Report Odoo 14.0](error-penulisan-format-report-odoo-14.png "Error Format Report Odoo 14.0")

Kamu hanya perlu mengubah penulisan XML nya menjadi seperti dibawah ini:

##### Format syntax Odoo versi lama.
```
<report
    id="penagihan_faktur"
    string="Penagihan Faktur"
    model="daftar.penagihan.faktur"
    report_type="qweb-pdf"
    paperformat="paperformat_penagihan_faktur"
    file="vit_penagihan_faktur.file_penagihan_faktur"
    name="vit_penagihan_faktur.file_penagihan_faktur"
    print_report_name="'Penagihan Faktur - %s' % (object.name)"
/>
```

##### Format syntax Odoo versi 14.0.
```
<record id="action_penagihan_faktur" model="ir.actions.report">
    <field name="name">Penagihan Faktur</field>
    <field name="model">daftar.penagihan.faktur</field>
    <field name="report_type">qweb-pdf</field>
    <field name="report_name">vit_penagihan_faktur.file_penagihan_faktur</field>
    <field name="report_file">vit_penagihan_faktur.file_penagihan_faktur</field>
    <field name="print_report_name">'Penagihan Faktur - %s' % (object.name)</field>
    <field name="binding_model_id" ref="model_daftar_penagihan_faktur"/>
    <field name="binding_type">report</field>
</record>
```