---
title: "Resolved : Ftp_put(): Can’t open that file: No such file or directory"
subtitle: ""
date: 2022-06-19T08:00:36+07:00
lastmod: 2022-06-19T08:00:36+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Resolved Ftp_put(): Can’t open that file: No such file or directory"

tags: []
categories: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-00webhost-ftpput-cant-open-that-file-no-such-file.jpg"
featuredImagePreview: "armannurhidayat-blog-00webhost-ftpput-cant-open-that-file-no-such-file.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Pernah mengalami error `ftp_put(): Can't open that file: No such file or directory` ketika ingin ekstrak file di Hosting gratis dari [000Webhost](https://id.000webhost.com) ?

kira-kira tampilan pesan errornya seperti dibawah ini:

![Ftp_put(): Can’t open that file: No such file or directory](error-ftpput-cant-open-that-file-no-such-file.png "Ftp_put(): Can’t open that file: No such file or directory")

Ini disebabkan karena adanya pembaharuan tampilan dashboard dan beberapa tambahan fitur di 000Webhost. Sebenarnya, untuk mengatasi masalah ini cukup mudah sekali dan berikut ini saya jelaskan langkah-langkahnya.

1. Download file [Unzipper.php](https://github.com/ndeet/unzipper)
![Download Unzipper.php](download-unzipper.png "Download Unzipper.php")

2. Setelah itu upload file `unzipper.php` dan file `.zip` project kamu didalam File Manager
![Upload File Unzipper.php](upload-file-unzipper.png "Upload File Unzipper.php")

3. Kemudian Akses URL domain kamu dan arahkan langsung ke file `unzipper.php`. Dan akan menampilkan halaman seperti berikut:
![Akses URL File Unzipper.php](akses-url-file-unzipper.png "Akses URL File Unzipper.php")

Untuk mengekstrak file `.zip` kamu hanya perlu memilih file yang ingin di ekstrak kemudian klik tombol `Unzip Archive`.