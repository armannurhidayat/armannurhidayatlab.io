---
title: "Panduan Lengkap Penggunaan Context Default Di Odoo"
subtitle: ""
date: 2024-10-30T18:17:04+07:00
lastmod: 2024-10-30T18:17:04+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: ""

tags: ["Odoo"]
categories: ["Odoo"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-panduan-lengkap-penggunaan-context-default-di-odoo.jpg"
featuredImagePreview: "armannurhidayat-blog-panduan-lengkap-penggunaan-context-default-di-odoo.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Dalam pengembangan Odoo, context adalah fitur yang sangat berguna untuk mengontrol bagaimana data dapat ditampilkan dan diolah dalam berbagai modul. Dengan menggunakan context default, kita dapat menetapkan nilai awal, mengelompokkan data, serta mengatur tampilan default tanpa perlu interaksi manual dari pengguna.

Artikel ini akan membahas secara mendalam bagaimana context default dapat digunakan untuk meningkatkan efisiensi di Odoo, baik melalui kode Python maupun XML.

#### Apa Itu Context Default di Odoo?

Context default merupakan parameter yang digunakan dalam Odoo untuk menetapkan nilai awal pada suatu field atau menentukan perilaku tampilan dalam suatu model. Ini sangat berguna untuk mengatur filter default, nilai default field, dan bahkan mengontrol bagaimana form atau list view ditampilkan.

Context dapat digunakan dalam berbagai skenario, seperti:
1. Menetapkan **default value** untuk field tertentu.
2. Mengatur **filter dan sorting** default dalam tampilan list.
3. Mengontrol **group by** pada tampilan list view.
4. Menyesuaikan **tampilan berdasarkan kondisi** pengguna atau role tertentu.

#### Cara Menggunakan Context Default di Odoo

##### 1. Menetapkan Default Value untuk Field dalam Python

Dalam kode Python, kita bisa menggunakan `default_` dibarengi dengan nama field untuk menetapkan nilai awal pada suatu field saat membuka form tertentu:

```python
class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def action_confirm(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sale.order',
            'view_mode': 'form',
            'context': {'default_partner_id': self.partner_id.id, 'default_currency_id': self.currency_id.id},
        }
```
Kode di atas akan mengisi field `partner_id` dan `currency_id` secara otomatis ketika form order dibuka.


##### 2. Mengatur Filter dan Group By Default dalam XML

Jika kita ingin mengelompokkan data secara otomatis saat tampilan dibuka, kita bisa menambahkan context dalam action di XML. Dengan menggunakan `search_default_` dibarengi dengan name filter.

Kamu bisa mengecek name filter di `Debug->Edit SearchView`.

![gambar edit search view](gambar_edit_searhview.png)

Name `draft` ini yang kita tambahkan.

![gambar name view](gambar_name_view.png)

Contoh penggunaan filter dengan status Quotation pada tampilan list:

```xml
<record id="action_sale_order" model="ir.actions.act_window">
    <field name="name">Sales Orders</field>
    <field name="res_model">sale.order</field>
    <field name="view_mode">tree,form</field>
    <field name="context">{'search_default_draft': 1}</field>
</record>
```

Kemudian tambahkan value `1` untuk mengaktifkan atau `0` untuk menonaktifkan. Dengan konfigurasi ini, data akan otomatis memfilter status Quotation saat pengguna membuka daftar Sales Orders.

Contoh penggunaan group by bulan pada tampilan list:

```xml
<record id="action_sale_order" model="ir.actions.act_window">
    <field name="name">Sales Orders</field>
    <field name="res_model">sale.order</field>
    <field name="view_mode">tree,form</field>
    <field name="context">{'search_default_groupby_date': ['date:month']}</field>
</record>
```

Sama dengan filter untuk menggunakan group by kita bisa menggunakan `search_default_` dibarengi dengan name filter, kemudian tambahkan value `['date:month']` untuk group by bulan atau `['date:year']` untuk group by tahun.


##### 3. Menyesuaikan Tampilan Berdasarkan Role Pengguna

Dalam beberapa kasus, kita ingin menyesuaikan tampilan berdasarkan hak akses pengguna. Berikut adalah contoh cara menyembunyikan harga beli bagi pengguna yang bukan Purchase Manager:

```xml
<record id="action_purchase_order" model="ir.actions.act_window">
    <field name="name">Purchase Orders</field>
    <field name="res_model">purchase.order</field>
    <field name="view_mode">tree,form</field>
    <field name="context">{'show_prices': user.has_group('purchase.group_manager')}</field>
</record>
```

Dengan cara ini, maka hanya pengguna dengan grup purchase manager yang dapat melihat informasi harga beli.

#### Kesimpulan

Menggunakan context default di Odoo dapat membantu meningkatkan produktivitas dengan:

✅ Menetapkan nilai awal otomatis.<br/>
✅ Mengelompokkan dan menyaring data dengan lebih efisien.<br/>
✅ Menyesuaikan tampilan berdasarkan hak akses pengguna.<br/>

Dengan memahami cara kerja context default, Kamu dapat mengoptimalkan sistem Odoo agar lebih sesuai dengan kebutuhan pengguna.