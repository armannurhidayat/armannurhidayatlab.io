---
title: "Resolved : Assets Odoo Hilang Atau Page Odoo Blank"
subtitle: ""
date: 2021-10-08T10:06:20+07:00
lastmod: 2021-10-08T10:06:20+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Page Odoo blank"

tags: ["Odoo"]
categories: ["Odoo"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-assets-odoo-hilang-atau-page-odoo-blank.jpg"
featuredImagePreview: "armannurhidayat-blog-assets-odoo-hilang-atau-page-odoo-blank.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Pernah mengalami page Odoo blank tiba tiba?
Kalau kamu pernah mengalaminya tidak perlu panik dan risau, hal ini sepengalaman saya biasanya terjadi setelah kita melalukan restore database yang di backup tanpa filestore.

<br/>
Langsung aja deh biar kamu gak terlalu lama pusing buat mikirin cara fixing nya... :smile:
<br/><br/>

Langkah pertama yang perlu kamu lakukan adalah masuk ke database kemudikan jalankan perintah SQL dibawah ini:

```sql
DELETE FROM ir_attachment WHERE url LIKE '/web/content/%';
```

Setelah menjalankan perintah SQL di atas, kamu bisa menjalankan ulang server Odoo dengan tambahan command `-d {DATABASE_NAME} -u base` untuk mengupgrade module base di Odoo. 