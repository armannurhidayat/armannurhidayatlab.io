---
title: "Belajar Python Dasar : Memahami Percabangan dengan Mudah"
subtitle: ""
date: 2021-10-22T20:18:09+07:00
lastmod: 2021-10-22T20:18:09+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "belajar python dasar memahami lebih dalam percabangan python dengan mudah"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-memahami-percabangan-dengan-mudah.jpg"
featuredImagePreview: "armannurhidayat-blog-memahami-percabangan-dengan-mudah.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

__Percabangan dalam Python__ adalah salah satu konsep dasar pemrograman yang sangat penting. Dengan percabangan, kita dapat membuat program yang dapat mengambil keputusan dan melakukan tindakan berbeda berdasarkan kondisi tertentu. Pada pembahasan kali ini, kamu akan mempelajari apa itu percabangan dalam Python, bagaimana cara menggunakannya, serta contoh-contoh percabangan sederhana yang bisa langsung kamu praktikkan.

#### Apa itu Percabangan dalam Python?

Percabangan merupakan struktur kontrol yang memungkinkan kode untuk mengeksekusi blok perintah tertentu hanya jika kondisi yang diberikan terpenuhi. Pada Python, kita dapat menggunakan beberapa pernyataan percabangan seperti `if`, `elif`, dan `else` untuk mengatur alur program berdasarkan kondisi tertentu.

#### Jenis-Jenis Percabangan di Python

Di Python, kita menggunakan beberapa jenis pernyataan untuk membuat percabangan, yaitu:

1. __Percabangan If__
2. __Percabangan If-Else__
3. __Percabangan If-Elif-Else__
4. __Percabangan Bersarang (Nested If)__

Saya coba jelaskan satu per satu dengan contoh kode penggunaanya.

##### 1. Percabangan `If` di Python

Percabangan `if` adalah bentuk percabangan dasar dalam Python. Kamu bisa menggunakan `if` untuk menjalankan kode jika kondisi yang diberikan bernilai benar (`True`).

```python
# Contoh percabangan If
umur = 18
if umur >= 18:
    print("Kamu sudah cukup umur untuk membuat KTP.")
```

__Penjelasan__: Jika variabel `umur` bernilai 18 atau lebih, program akan mencetak pesan `"Kamu sudah cukup umur untuk membuat KTP."`

##### 2. Percabangan `If-Else` di Python

Jika kamu ingin memberikan alternatif lain ketika kondisi `if` tidak terpenuhi, kamu dapat menggunakan `if-else`. Dengan `else`, kita bisa menentukan aksi yang dijalankan ketika kondisi `if` bernilai `False`.

```python
# Contoh If-Else
nilai = 70
if nilai >= 75:
    print("Selamat, Anda lulus!")
else:
    print("Maaf, Anda belum lulus.")
```

__Penjelasan__: Jika variabel `nilai` lebih besar dari 75, maka akan mencetak pesan `"Selamat, Anda lulus!"`. Jika tidak, maka akan mencetak `"Maaf, Anda belum lulus."`.

##### 3. Percabangan `If-Elif-Else` di Python

Pada kasus tertentu kita butuh mengecek lebih dari satu kondisi. Untuk itu, gunakan `if-elif-else` agar program bisa memilih di antara beberapa pilihan.

```python
# Contoh If-Elif-Else
skor = 85
if skor >= 90:
    print("Nilai Anda A")
elif skor >= 80:
    print("Nilai Anda B")
elif skor >= 70:
    print("Nilai Anda C")
else:
    print("Nilai Anda D")
```

__Penjelasan__: Program akan mengecek nilai skor dan memberikan penilaian yang sesuai. Jika skor 90 atau lebih, maka akan mencetak `"Nilai Anda A"`, dan seterusnya sesuai kondisi yang dibuat.

##### 4. Percabangan Bersarang (Nested If) di Python

Percabangan bersarang adalah percabangan di dalam percabangan. Hal ini dapat digunakan ketika kamu ingin mengecek kondisi tambahan setelah kondisi pertama.

```python
# Contoh Nested If
usia = 20
status_mahasiswa = True

if usia > 18:
    if status_mahasiswa:
        print("Anda adalah mahasiswa di atas 18 tahun.")
    else:
        print("Anda bukan mahasiswa, namun berusia di atas 18 tahun.")
else:
    print("Anda berusia di bawah 18 tahun.")
```

__Penjelasan__: Program ini pertama-tama mengecek apakah `usia` lebih dari 18. Jika ya, maka akan mengecek status sebagai mahasiswa. Jika tidak, program langsung mengakhiri dengan `"Anda berusia di bawah 18 tahun."`

#### Mengapa Percabangan Penting dalam Pemrograman Python?

Percabangan membuat kode lebih fleksibel dan interaktif. Dalam pengembangan aplikasi, kamu akan sering menggunakan percabangan untuk berbagai keperluan, seperti memvalidasi inputan dari pengguna, memberikan respon yang berbeda, atau mengarahkan logika program.

#### Kesimpulan

Belajar percabangan dalam Python merupakan langkah penting bagi siapa saja yang baru memulai pemrograman. Dengan menguasai `if`, `else`, `elif`, dan percabangan bersarang, kamu sudah selangkah lebih maju dalam membuat program yang logis dan interaktif. Jangan ragu untuk mencoba contoh di atas dan eksplorasi lebih lanjut program yang kamu buat.