---
title: "Belajar Python Dasar : Tipe Data Dictionary"
subtitle: ""
date: 2021-10-20T20:15:53+07:00
lastmod: 2021-10-20T20:15:53+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Tipe data dictionary di python"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-python-tipe-data-dictionary.jpg"
featuredImagePreview: "armannurhidayat-blog-python-tipe-data-dictionary.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Di Python sendiri terdapat 4 tipe data collection yang digunakan untuk menyimpan kumpulan data dalam satu variabel diantaranya yaitu [List](/python-tipe-data-list), [Tuple](/python-tipe-data-tuple), [Set](/python-tipe-data-set) dan Dictionary tentunya dengan cara penggunaan yang berbeda-beda. Namun pada artikel kali ini saya akan fokus membahas tipe data Dictionary.

Dictionary merupakan tipe data pada Python yang berfungsi untuk menyimpan kumpulan data atau nilai, yang setiap urutanya berisi key dan value. Jika biasanya kita ingin mengakses nilai pada _list_ menggunakan indeks, di dictionary ini kita perlu kata kunci (key) untuk mengakses nilainya.

### Membuat Dictionary
Untuk membuat tipe data dictionary berbeda dengan List atau Tuple. Karena kumpulan nilai di dictionary menggunakan key dan value, maka setiap key dipisahkan dari value-nya dengan tanda titik dua `:`, setiap nilai dipisahkan tanda koma `,`  dan di bungkus menggunakan tanda kurung kurawal `{}`.

Untuk nilai dari value bisa di isi dengan tipe data apapun (string, list, boolean, dictionary, dll), sedangkan untuk key harus berupa tipe data yang tidak berubah seperti string atau number. Perlu di perhatikan juga key bersifat _case-sensitive_.

Contoh:
```python
artikel = {
  'title'      : 'Belajar Python Dasar : Tipe Data Dictionary',
  'url'        : 'https://armannurhidayat.com',
  'date'       : '2021-10-20',
  'draft'      : False,
  'categories' : ['Programming', 'Python']
}

print(artikel)
```

Hasil output:

    {
      'title'      : 'Belajar Python Dasar : Tipe Data Dictionary',
      'url'        : 'https://armannurhidayat.com',
      'date'       : '2021-10-20',
      'draft'      : False,
      'categories' : [
        'Programming',
        'Python'
      ]
    }

Pada contoh di atas kita telah membuat dictionary dengan nama `artikel` yang berisikan data `title`, `url`, `date`, `draft`, dan `categories`. Itu semua merupakan kunci (key) yang bisa kita gunakan untuk mengakses value di dalamnya.

### Mengakses Nilai Dictionary
Cara mengakses nilai dictionary ada 2 cara yaitu dengan menggunakan kurung siku `[key]` kemudian menambahkan key didalam nya atau bisa juga menggunakan fungsi `get(key)`.

Contoh:
```python
# membuat dictionary
artikel = {
  'title'      : 'Belajar Tipe Data Dictionary',
  'url'        : 'https://armannurhidayat.com',
  'date'       : '2021-10-20',
  'draft'      : False,
  'categories' : ['Programming', 'Python'],
  'author'     : {
    'name'    : 'arman',
    'address' : 'Bandung'
  }
}

# mengakses nilai dictionary dengan kurung siku []
print('Judul: ', artikel['title'])
print('Penulis: ', artikel['author']['name'])

# mengakses nilai dictionary dengan fungsi get()
print('url: ', artikel.get('url'))
print('date: ', artikel.get('date'))

```

Hasil output:

    Judul: Belajar Tipe Data Dictionary
    Penulis: arman
    url: https://armannurhidayat.com
    date: 2021-10-20

Perbedaan dari ke-2 cara mengakses nilai di atas adalah, dengan menggunakan fungsi `get()` kita bisa memberikan nilai default jika key pada dictionary tidak ditemukan. Hal ini untuk menghindari sistem menampilkan pesan error. Berikut contoh penggunaannya:

```python
artikel = {
  'title'   : 'Belajar Tipe Data Dictionary',
  'author'  : {
    'name'    : 'arman',
    'address' : 'Bandung'
  }
}

# akan menampilkan error
print(artikel['author']['alamat'])

# akan menampilkan nilai default Indonesia
print(artikel.get('author').get('alamat', 'Indonesia'))
```

### Menggunakan Perulangan
Jika ingin menampilkan semua nilai dari sebuah dictionary kita bisa menggunakan perulangan.

Contoh:
```python
siswa = {
  'nama'  : 'budi',
  'kelas' : 'A',
  'umur'  : 17,
  'alamat': 'Jl. nin aja dulu',
}

for key in siswa:
  print(siswa[key])
```

Hasil output:
    
    budi
    A
    17
    Jl. nin aja dulu

Atau bisa juga melakukan perulangan menggunakan fungsi `dictionary.items()` untuk mendapatkan key dan value sekaligus dari sebuah dictionary.

Contoh:
```python
siswa = {
  'nama'  : 'budi',
  'kelas' : 'A',
  'umur'  : 17,
  'alamat': 'Jl. nin aja dulu',
}

for key, value in siswa.items():
  print(key, '=>', value)
```

Hasil output:
    
    nama => budi
    kelas => A
    umur => 17
    alamat => Jl. nin aja dulu

### Menambah Dan Mengubah Nilai Dictionary
Untuk menambahkan dan mengubah nilai dictionary, keduanya menggunakan cara yang sama. Jadi jika key yang di definisikan sudah ada akan melakukan proses update sedangkan jika belum ada akan melakukan proses penambahan nilai baru.

Contoh:
```python
siswa = {
  'nama'  : 'budi',
  'kelas' : 'A',
  'umur'  : 17,
  'alamat': 'Jl. nin aja dulu',
}

# Menambah nilai
siswa['hobi'] = 'ngoding'

# Mengubah nilai
siswa['kelas'] = 'B'

print(siswa)
```

Hasil output:
    
    {
      'nama': 'budi',
      'kelas': 'B',
      'umur': 17,
      'alamat': 'Jl. nin aja dulu',
      'hobi': 'ngoding'
    }

Bisa kita lihat dari hasil output di atas, terdapat nilai dictionary baru yaitu `'hobi': 'ngoding'` dan mengubah value dari key `kelas` yang sebelumnya `A` menjadi `B`.

### Menghapus Nilai Dictionary
Untuk menghapus nilai dictionary ada 2 cara yaitu menggunakan statement `del` atau dengan fungsi `pop()`. Perbedaan dari kedua fungsi ini adalah, dengan fungsi `pop()` kita bisa mendapatkan nilai kembaliannya.

Contoh:
```python
siswa = {
  'nama'  : 'budi',
  'kelas' : 'A',
  'umur'  : 17,
  'alamat': 'Jl. nin aja dulu',
}

# dengan statement del
del siswa['nama']

# dengan fungsi pop()
siswa.pop('kelas')
```

Atau jika ingin menghapus semua dictionary bisa menggunakan fungsi `dictionary.clear()`.

### Menghitung Banyak Nilai Dictionary
Kita juga bisa mengetahui banyaknya nilai yang terdapat pada dictionary dengan menggunakan fungsi `len()`.

Contoh:
```python
jeruk = {
  'kalori' : '47',
  'Air' : '87%',
  'protein' : '0,9 gram',
  'karbohidrat' : '11,8 gram',
  'gula' : '9,4 gram',
  'serat' : '2,4 gram',
  'lemak' : '0,1 gram',
}

print('total kandungan pada jeruk : {}'.format(len(jeruk)))
```

Hasil output:

    total kandungan pada jeruk : 7