---
title: "Interfacing Python Dengan Zebra Barcode Printer"
subtitle: ""
date: 2021-11-27T11:06:39+07:00
lastmod: 2021-11-27T11:06:39+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Interfacing python dengan zebra barcode printer"

tags: ["Programming", "Python"]
categories: ["Python"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-interfacing-python-dengan-zebra-barcode-printer.jpg"
featuredImagePreview: "armannurhidayat-blog-interfacing-python-dengan-zebra-barcode-printer.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Printer Zebra saat ini menggunakan bahasa pemrograman ZPL2 yang dikembangkan oleh Zebra Technologies. ZPL2 ini telah memiliki lebih dari 170 perintah dan Commands dari bahasa ZPL2 itu sendiri selalu dimulai dengan simbol caret `^`.

Command ZPL2:

```ZPL2
^XA
^FO300,300
^BQN,2,10,M^FDarmannurhidayat.com^FS
^XZ
```

Untuk interfacing python ke zebra barcode printer cukup mudah, karena sudah banyak sekali package Python yang sangat memudahkan developer untuk melakukan encoding ZPL2, Seperti zpl atau simple_zpl2. Sedangkan pada artikel kali ini saya akan fokus membahas package simple_zpl2.

Pertama untuk install Simple ZPL2, jalankan command dibawah melalui terminal.

```bash
pip install simple_zpl2
```

Contoh script untuk membuat QR Code menggunakan Simple ZPL2:

```python
from simple_zpl2 import ZPLDocument, QR_Barcode

# membuat object dari class ZPLDocument
zpl = ZPLDocument()

# mengatur posisi qrcode
zpl.add_field_origin(300, 300)

# isi dari qrcode
qr_data = 'QA, TEST QR Code'
# Update: tambahan 'QA,' sebelum text QR Code bertujuan untuk menghindari
# tiga karakter awal yang hilang.

# mengatur model dan ukuruan qrcode
qr = QR_Barcode(qr_data, 2, 10, zpl._QR_ERROR_CORRECTION_STANDARD)
zpl.add_barcode(qr)
```

Untuk menampilkan hasil text yang sudah di encoding, bisa menggunakan cara dibawah ini:

```python
print(zpl.zpl_text)
```

![Hasil Encoding](preview-encoded.png "Hasil Encoding")

Jika kamu ingin merender label dengan format PNG terlebih dahulu, untuk mengetahui hasil labelnya sebelum di print langsung, bisa dengan cara berikut:

```python
from PIL import Image
import io

# Mengatur ukuran kertas label
png = zpl.render_png(label_width=4, label_height=6)
file = io.BytesIO(png)
img = Image.open(file)

# Membuka gambar dan penampil gambar default di sistem Kamu
img.show()
```

Untuk melakukan test print melalui terminal, kamu bisa menjalankan command seperti berikut:
```bash
python FILE_PRINT.py | lpr -l
```

{{< admonition note >}}
Jika tidak menambahkan command `-l` hasil print akan berantakan atau tidak akan diterjemahkan oleh sistem.
{{< /admonition >}}


### Text Orientation
Ketika kamu ingin mengubah rotasi pada text, kamu hanya perlu mengganti parameter yang sudah disediakan. Berikut beberapa contoh yang bisa digunakan untuk mengubah rotasi:

```python
# Orientation Normal
zpl.add_field_origin(300, 100)
zpl.add_font('C', zpl._ORIENTATION_NORMAL, 10)
zpl.add_field_data('armannurhidayat.com')

# Mengubah rotasi text 90°
zpl.add_field_origin(300, 100)
zpl.add_font('C', zpl._ORIENTATION_90, 10)
zpl.add_field_data('armannurhidayat.com')

# Mengubah rotasi text 180°
zpl.add_field_origin(300, 100)
zpl.add_font('C', zpl._ORIENTATION_180, 10)
zpl.add_field_data('armannurhidayat.com')

# Mengubah rotasi text 270°
zpl.add_field_origin(300, 100)
zpl.add_font('C', zpl._ORIENTATION_270, 10)
zpl.add_field_data('armannurhidayat.com')
```


{{< admonition note >}}
Sedangkan jika ingin menghubungkan dengan Website Odoo kamu perlu membuat aplikasi tersendiri sebagai jembatan antara Odoo dengan zebra printer menggunakan flask.
{{< /admonition >}}

Untuk dokumentasi Simple ZPL2 lebih lengkap, kamu bisa mengunjungi website resminya disini: [https://simple-zpl2.readthedocs.io](https://simple-zpl2.readthedocs.io).

{{< admonition note "Odoo Apps" >}}
[Module Zebra Printer Integration](https://apps.odoo.com/apps/modules/12.0/syntax_zebra_printer/)
{{< /admonition >}}

<!-- ref -->
<!-- https://selfhelp.windwardoncloud.com/System5Zebra_ZPL2_Samples.htm -->
<!-- https://www.zebra.com/us/en/support-downloads/knowledge-articles/ait/QR-code-missing-first-three-characters-when-using-ZPL-language.html -->