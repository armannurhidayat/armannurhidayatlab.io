---
title: "Belajar Python Dasar : Tipe Data Tuple"
subtitle: ""
date: 2021-10-20T20:04:34+07:00
lastmod: 2021-10-20T20:04:34+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Tipe data tuple di python"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-python-tipe-data-tuple.jpg"
featuredImagePreview: "armannurhidayat-blog-python-tipe-data-tuple.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Di Python sendiri terdapat 4 tipe data collection yang digunakan untuk menyimpan kumpulan data dalam satu variabel diantaranya yaitu [List](/python-tipe-data-list), Tuple, [Set](/python-tipe-data-set) dan [Dictionary](/python-tipe-data-dictionary) tentunya dengan cara penggunaan yang berbeda-beda. Namun pada artikel kali ini saya akan fokus membahas tipe data Tuple.

Tuple bersifat _immutable_, yang artinya isi tuple tidak bisa kita ubah tidak seperti list di Python. Namun tuple, dapat kita isi dengan berbagai macam nilai dan datanya bisa kita akses menggunakan indeks.

### Membuat Tuple
Untuk membuat tipe data tuple didefinisikan dengan tanda kurung `()` ini opsional, karena dengan kita membuat variabel dan memberikan tipe data string atau integer yang dipisahkan dengan tanda koma `,` Python akan membacanya sebagai tuple.

Contoh:
```python
tuple1 = ('Mobil', 'Rumah', 'Apartemen')
tuple2 = 'Mobil', 'Rumah', 'Apartemen'
tuple3 = 1, 2, 3

# menampilkan data tuple
print(tuple1)
print(tuple2)
print(tuple3)

# mengecek tipe data tuple
print(type(tuple1))
print(type(tuple2))
print(type(tuple3))
```

Hasil output:

    ('Mobil', 'Rumah', 'Apartemen')
    ('Mobil', 'Rumah', 'Apartemen')
    (1, 2, 3)
    <class 'tuple'>
    <class 'tuple'>
    <class 'tuple'>

Sedangkan untuk tipe data tuple yang hanya memiliki satu nilai, harus di tambahkan tanda koma `,` di akhir nilai jika tidak akan di anggap sebagai tipe data string atau integer biasa.

Contoh:
```python
tuple1 = ('Mobil',)
tuple2 = 'Mobil',
tuple3 = 1,

# mengecek tipe data tuple
print(type(tuple1))
print(type(tuple2))
print(type(tuple3))
```

Hasil output:

    <class 'tuple'>
    <class 'tuple'>
    <class 'tuple'>

### Mengakses Nilai Tuple
Mengakses nilai yang ada pada tuple tidak jauh berbeda dengan cara mengakses nilai pada list, tuple juga memiliki indeks yang dimulai dari nol `0`.

Contoh:
```python
tuple1 = ('Kucing', 'Ayam')

print(tuple1[0])
print(tuple1[1])
```

Setelah mengeksekusi contoh kode di atas, maka hasil outputnya seperti dibawah ini:

    Kucing
    Ayam

Selain itu kita juga bisa mengakses nilai tuple dari kanan menggunakan negatif indeks.

Contoh:
```python
tuple1 = ('Kucing', 'Ayam')

print(tuple1[-1])
print(tuple1[-2])
```

Hasil output:

    Ayam
    Kucing

Cara lain Indexing, Slicing pada Python. Dengan contoh data seperti berikut:
`abjad = ('A', 'B', 'C', 'D')`

| Python Expression  | Output              | Penjelasan                                   
|:------------------ |:------------------- | :------------------------------------------
| `abjad[1]`         | `B`                 | Mengambil nilai dari kiri dimulai dari `0`
| `abjad[-1]`        | `D`                 | Mengambil nilai dari kanan dimulai dari `1`
| `abjad[2:]`        | `('A', 'B')`        | Mengambil nilai sebagian

### Sequence Unpacking
Dengan tuple kita juga bisa memecah isi data tuple dan menyimpannya ke dalam variabel-variabel yang berurutan.

Contoh:
```python
data = ('Arman', 17, 'Bandung')

# sequence unpacking
nama, umur, kota = data

print('Nama : ', nama)
print('Umur : ', umur)
print('Kota : ', kota)
```

Hasil output:

    Nama : Arman
    Umur : 17
    Kota : Bandung

### Mengubah Nilai Tuple
Seperti yang sudah saya singgung di atas bahwa tuple memiliki sifat _immutable_, tidak bisa diubah. Tetapi jika kalian menemukan data tuple dan tetap ingin mengubah isi nilainya bisa saja di akali dengan mengkonversi tuple ke list terlebih dahulu.

Contoh:
```python
warna = ('Putih', 'Hitam', 'Merah')

# konversi tuple ke list
list_warna = list(warna)

# merubah nilai Hitam ke Biru
list_warna[1] = 'Biru'

# konversi list ke tuple
tuple_warna = tuple(list_warna)

print(tuple_warna)
```

Hasil output `('Putih', 'Biru', 'Merah')`

### Menggabungkan Tuple
Kita juga bisa menggabungkan lebih dari satu tuple menjadi satu tuple baru dengan menggunakan operator penjumlahan `+`.

Contoh:
```python
angka = (1 , 2, 3)
abjad = ('a', 'b', 'c')

tuple_baru = angka + abjad
print(tuple_baru)
```

Hasil output `(1, 2, 3, 'a', 'b', 'c')`

### Fungsi-fungsi Pada Tuple
Python menyediakan fungsi-fungsi built-in yang bisa digunakan seperti berikut:

| Fungsi     | Keterangan
|:---------- |:------------------------------------
| `len()`    | Menghitung jumlah item pada tuple
| `max()`    | Mengetahui nilai terbesar pada tuple
| `min()`    | Mengetahui nilai terkecil pada tuple

Contoh penggunaan:
```python
my_tuple = (3, 2, 1, 9)

print(len(my_tuple))
print(max(my_tuple))
print(min(my_tuple))
```