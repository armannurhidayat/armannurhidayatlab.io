---
title: "Cara Menambahkan Icon Pada Module Baru Odoo"
subtitle: ""
date: 2022-01-07T20:15:16+07:00
lastmod: 2022-01-07T20:15:16+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Bagaimana cara menambahkan icon pada module baru di odoo"

tags: ["Odoo"]
categories: ["Odoo"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-cara-menambahkan-pada-icon-module-baru-odoo.jpg"
featuredImagePreview: "armannurhidayat-blog-cara-menambahkan-pada-icon-module-baru-odoo.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

1. Buat folder baru dengan nama 'static' di dalam module yang sudah dibuat.
2. Di dalam folder 'static' buat folder baru lagi dengan nama 'description'.
3. Tambahkan file gambar di dalam folder '/static/description' dan pastikan file gambar dalam format '.png' kemudian rename gambar menjadi : icon.png

![lokasi gambar](menambahkan-icon-pada-module-odoo.png "lokasi gambar")

4. Restart server Odoo, dan module baru kamu sudah memiliki icon.

![Preview](preview-menambahkan-icon-pada-module-odoo.png "Preview")