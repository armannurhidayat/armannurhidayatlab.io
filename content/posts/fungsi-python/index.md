---
title: "Belajar Python Dasar : Fungsi Python"
subtitle: ""
date: 2021-10-21T20:18:09+07:00
lastmod: 2021-10-21T20:18:09+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "belajar python dasar cara penggunaan fungsi python"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-fungsi-python.jpg"
featuredImagePreview: "armannurhidayat-blog-fungsi-python.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Fungsi merupakan salah satu konsep penting yang ada dalam pemrograman Python. Fungsi memungkinkan kita untuk mengelompokkan blok kode yang bisa digunakan kembali di berbagai bagian program, sehingga membuat kode menjadi lebih terstruktur, mudah dibaca, dan efisien. Mari kita pelajari lebih dalam tentang apa itu fungsi, bagaimana cara membuat, dan cara menggunakannya.

<br/>

#### Apa itu Fungsi?

Fungsi adalah sekumpulan instruksi yang diberi nama dan dapat dipanggil (dijalankan) kapan saja dalam program. Dengan fungsi, kita bisa membagi kode menjadi bagian-bagian kecil yang dapat dipanggil berulang kali tanpa harus menuliskannya kembali. Sehingga kode yang kita buat lebih clean.

{{< admonition quote >}}
"Orang bodoh mana pun dapat menulis kode yang dapat dipahami komputer. Programmer yang baik menulis kode yang dapat dipahami manusia." – Martin Fowler
{{< /admonition >}}


#### Mengapa Menggunakan Fungsi?
Beberapa alasan menggunakan fungsi dalam pemrograman:
* __Menyederhanakan kode__: Kode yang kompleks bisa dipecah menjadi fungsi-fungsi yang lebih kecil.

* __Menghindari duplikasi__: Fungsi bisa digunakan kembali di berbagai tempat dalam program tanpa perlu kita menulis ulang kode yang sudah ada.

* __Mudah diperbaiki__: Jika ada kesalahan, cukup memperbaiki fungsi yang salah dan semua bagian yang menggunakan fungsi itu akan terupdate.


#### Cara Membuat Fungsi di Python
Fungsi dideklarasikan dengan menggunakan kata kunci `def`, diikuti oleh nama fungsi, tanda kurung `()`, dan diakhiri dengan titik dua `:` di dalamnya, kamu bisa menulis blok kode yang akan dijalankan saat fungsi dipanggil.

#### Contoh Sederhana Membuat Fungsi
Berikut contoh sederhana membuat fungsi di Python:

```python
def salam():
  print("Halo, selamat datang di Belajar Python Dasar!")
```

Fungsi di atas bernama `salam`. Saat kita memanggilnya dengan cara `salam()`, fungsi ini akan menampilkan teks `"Halo, selamat datang di Belajar Python Dasar!"`

#### Memanggil Fungsi
Untuk menjalankan fungsi yang sudah kita buat, cukup panggil nama fungsinya diikuti tanda kurung `()`.

```python
# Memanggil fungsi salam
salam()
```

Hasilnya:

    Halo, selamat datang di Belajar Python Dasar!

#### Fungsi dengan Parameter

Parameter adalah variabel yang bisa diteruskan ke fungsi untuk memberikan input tertentu. Dengan parameter, kita bisa membuat fungsi yang lebih fleksibel.

```python
def salam(nama):
  print(f"Halo, {nama}! Selamat datang di Belajar Python Dasar!")

# Memanggil fungsi dengan memberikan nama sebagai parameter
salam("Budi")
```

Hasilnya:

    Halo, Budi! Selamat datang di Belajar Python Dasar!


#### Fungsi dengan Nilai Kembali (Return)

Kadang-kadang kita ingin fungsi mengembalikan nilai. Untuk ini, kita bisa menggunakan pernyataan `return`.

```python
def tambah(a, b):
  return a + b

# Menggunakan fungsi tambah
hasil = tambah(5, 3)
print("Hasil penjumlahan:", hasil)
```

Hasilnya:

    Hasil penjumlahan: 8

#### Fungsi dengan Parameter Default

Kita bisa menetapkan nilai default untuk parameter, sehingga jika parameter tidak diisi, Python akan menggunakan nilai default tersebut.


```python
def salam(nama="teman"):
  print(f"Halo, {nama}! Selamat datang di Belajar Python Dasar!")

# Memanggil fungsi tanpa parameter
salam()

# Memanggil fungsi dengan parameter
salam("Budi")
```

Hasilnya:

    Halo, teman! Selamat datang di Belajar Python Dasar!
    Halo, Budi! Selamat datang di Belajar Python Dasar!

#### Rangkuman
1. __Fungsi__ adalah blok kode yang bisa dipanggil berulang kali.
2. __Definisi__ fungsi menggunakan kata kunci `def` diikuti dengan nama fungsi.
3. __Parameter__ adalah input yang bisa diberikan ke fungsi untuk menghasilkan keluaran tertentu.
4. __Return__ digunakan untuk mengembalikan nilai dari fungsi.

Dengan memahami fungsi, kamu akan bisa menulis program Python yang lebih modular dan mudah di-maintain. Selamat mencoba membuat dan menggunakan fungsi dalam Python! :handshake: