---
title: "Belajar Python Dasar : Cara Menjalankan File Python"
subtitle: ""
date: 2021-10-11T09:41:00+07:00
lastmod: 2021-10-11T09:41:00+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "cara menjalankan file python"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-cara-menjalankan-file-python.jpg"
featuredImagePreview: "armannurhidayat-blog-cara-menjalankan-file-python.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Pada materi sebelumnya kita telah berhasil melakukan [Instalasi Python Di Windows](/instalasi-python-di-windows/). Di materi kali ini saya akan membahas bagaimana cara menjalankan file Python dengan dua mode yang bisa digunakan, yaitu _Interactive mode_ (CMD/Terminal) dan _Script mode_ (Pycharm/Vscode) beserta perbedaan dari dua mode tersebut.

Python merupakan bahasa pemrograman yang dieksekusi menggunakan interpreter. Karena setiap bahasa pemrograman _high level_ atau yang bisa di baca oleh manusia akan diubah terlebih dahulu menjadi bahasa mesin atau _low level_. Untuk mengubah _high level_ pun dibagi menjadi dua yaitu dengan interpreter atau compiler.

Interpreter merubah bahasa _high level_ ke bahasa yang bisa di baca oleh mesin dengan cara membaca kode baris perbaris. Sedangkan compiler merubah bahasa _high level_ ke bahasa mesin dengan membaca keseluruhan blok kemudian akan di eksekusi. Jika terdapat kesalahan pada kode maka program tidak akan berjalan.

## Interactive mode
Python adalah salah satu jenis bahasa yang menggunakan interpreter sehingga Python dapat dijalankan menggunakan _Interactive mode_. _Interactive mode_ akan mengeksekusi program atau menjalankan kode per baris. Metode ini biasa digunakan untuk menjalankan kode Python yang singkat dengan menggunakan command prompt (Windows) atau terminal (Linux).

Untuk menjalankan script Python menggunakan _interactive mode_, caranya buka command prompt atau terminal kemudian ketikkan `python`, maka akan tampil seperti ini:

![Cara menjalankan file python](cara-menjalankan-file-python-interactive-mode-01.png)

Halaman di atas merupakan halaman lingkungan Python, kamu sudah bisa menulis script Python disana. Contoh untuk menampilkan kalimat di python dapat menggunakan fungsi `print()` seperti ini:

```python
print("Hello Python...")
```

Jika sudah tekan tombol `ENTER`, dan script Python akan dijalankan.

![Cara menjalankan file python](cara-menjalankan-file-python-interactive-mode-02.png)

Setiap kali kamu selesai menulis kode Python dalam baris, kamu perlu menekan tombol `ENTER`. Sedangkan untuk keluar dari lingkungan Python kamu cukup mengetikkan `exit()` atau `control+D` pada MacOS.

Pada kenyataanya untuk membuat sebuah program tidak hanya membutuhkan beberapa baris saja, tetapi berbaris-baris kode. Oleh karena itu _interactive mode_ tidak disarankan untuk digunakan jika kamu akan membuat program yang lebih kompleks.

## Script mode
Dengan _script mode_ untuk menjalankan kode Python perlu disimpan dalam file kemudian dieksekusi melalui command prompt atau terminal. File harus berektensi `.py` ini menandakan bahwa file tersebut merupakan file Python.

Pertama kamu perlu membuka IDE (Integrated Development Environtment) seperti Pycharm atau Visual Studio Code. Kemudian kamu bisa mengetikkan contoh kode seperti ini:

```python
nama = "arman"
alamat = "bandung"
print("Halo, nama saya " + nama + " dan alamat saya di " + alamat)
```

Untuk menjalankan kode di atas kamu bisa menggunakan command prompt atau terminal dengan perintah `python print.py`. Perlu di perhatikan `print.py` disini bisa di sesuaikan dengan nama file yang sudah kamu buat.

![Cara menjalankan file python](cara-menjalankan-file-python-script-mode-01.png)

Dan lihat dibagian bawah hasil dari kode yang tadi kita buat menampilkan sebuah kalimat `"Halo, nama saya arman dan alamat saya di bandung`, ini tandanya kita sudah berhasil menjalankan file python dengan _script mode_.

Pada hari ini kita sudah belajar banyak mengenai cara menjalankan file Python dengan _Interactive mode_ dan _script mode_. Mungkin cukup sekian dulu pembahasan dari seri belajar python dasar hari ini, sampai jumpa di episode selanjutnya. :wave: