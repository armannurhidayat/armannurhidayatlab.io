---
title: "Belajar Python Dasar : Tipe Data List"
subtitle: ""
date: 2021-10-19T17:22:58+07:00
lastmod: 2021-10-19T17:22:58+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Tipe data list di python"

tags: ["Programming", "Python"]
categories: ["Belajar Python Dasar"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-tipe-data-list-python.jpg"
featuredImagePreview: "armannurhidayat-blog-tipe-data-list-python.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Pada pembahasan sebelumnya kita telah [mengenal variabel dan tipe data di Python](/mengenal-variabel-dan-tipe-data). Tetapi, disana saya hanya membahas dan menggunakan contoh tipe data yang sederhananya saja.

Di Python sendiri terdapat 4 tipe data collection yang digunakan untuk menyimpan kumpulan data dalam satu variabel diantaranya yaitu List, [Tuple](/python-tipe-data-tuple), [Set](/python-tipe-data-set) dan [Dictionary](/python-tipe-data-dictionary) tentunya dengan cara penggunaan yang berbeda-beda. Namun pada artikel kali ini saya akan fokus membahas tipe data List.

### Membuat List
Untuk membuat tipe data list, hanya perlu menambahkan tanda kurung siku `[]` pada nilai variabel yang telah dibuat, jika item yang disimpan lebih dari satu list perlu dipisah dengan tanda koma `,`.

```python
# list kosong
my_list = []

# list berisi satu item
my_list = ["Python"]

# list berisi kumpulan string
my_list = ["apel", "pisang", "mangga"]

# list berisi kumpulan integer
my_list = [1, 22, 31, 4, 12]

# list dengan tipe data campur
my_list = [1, True, "apple", 0.2]
```

Untuk menampilkan semua isi dari list di atas bisa menggunakan fungsi `print(my_list)`.

### Mengakses Nilai List
Untuk mengakses nilai tertentu dari list bisa menggunakan indeks. Karena setiap data yang disimpan pada list memiliki indeks sebagai alamatnya. Indeks merupakan nilai integer yang dimulai dari nol `0`.

       0        1         2         3
       :arrow_up_down:       :arrow_up_down:        :arrow_up_down:         :arrow_up_down:
    ['apel', 'pisang', 'mangga', 'jeruk']

Contoh:
```python
# data buah
buah = ['apel', 'pisang', 'mangga', 'jeruk']

# untuk menampilkan data pisang yang ada pada list buah,
# indeksnya adalah 1
print(buah[1])
```

Hasil output dari contoh script di atas adalah `pisang`

Cara lain Indexing, Slicing pada Python. Dengan contoh data seperti berikut:
`buah = ['apel', 'jeruk', 'ceri', 'pir']`

| Python Expression  | Output             | Penjelasan                                   
|:------------------ |:------------------ | :------------------------------------------
| `buah[2]`          | `ceri`             | Mengambil nilai dari kiri dimulai dari `0`
| `buah[-1]`         | `pir`              | Mengambil nilai dari kanan dimulai dari `1`
| `buah[2:]`         | `['ceri','pir']`   | Mengambil nilai sebagian


### Menambahkan Nilai List
Terdapat dua fungsi pada Python, untuk menambahkan nilai ke dalam list:

##### 1. `list.append(nilai)`
Fungsi `append()` hanya membutuhkan satu parameter, parameter tersebut merupakan nilai baru yang akan ditambahkan ke dalam list, dan nilai baru tersebut nantinya akan berada di urutan terakhir.

Contoh:
```python
warna = ['Merah', 'Hitam', 'Biru']

warna.append('Hijau')

print(warna)
```

Hasil output `['Merah', 'Hitam', 'Biru', 'Hijau']`

##### 2. `list.insert(indeks, nilai)`
Selain fungsi `append()` di Python, bisa juga menambahkan nilai sesuai urutuan yang kita mau. Dengan menggunakan fungsi `insert()`. Fungsi `insert()` membutuhkan dua buah parameter, parameter pertama untuk mendefinisikan indeks dari data baru yang akan dimasukkan, dan parameter kedua adalah nilai baru yang akan ditambahkan.

Contoh:
```python
warna = ['Merah', 'Hitam', 'Biru']

# menambahkan nilai Hijau di index ke-2
warna.insert(2, 'Hijau')

print(warna)
```

Hasil output `['Merah', 'Hitam', 'Hijau', 'Biru']`

### Mengubah Nilai List
Selain dapat menambahkan nilai, kita juga bisa mengubah nilai yang ada pada list, ini karena list merupakan tipe data yang _mutable_ atau bisa diubah.

Contoh:
```python
# nilai awal list
warna = ['Merah', 'Hitam', 'Biru']

# mengubah nilai indeks ke-1 yaitu Hitam menjadi Jingga
warna[1] = 'Jingga'

print(warna)
```

Hasil output `['Merah', 'Jingga', 'Biru']`

### Menghapus Nilai List
Untuk menghapus nilai yang ada pada list, kita bisa menggunakan fungsi `remove()`, `pop()`, dan `clear()`.

##### 1. `list.remove(nilai)`
Fungsi `remove()` membutuhkan satu parameter, parameter tersebut merupakan nilai yang akan di hapus. Jika terdapat nilai yang sama didalam list fungsi `remove()` akan menghapus satu nilai dari depan.

Contoh:
```python
bahasa = ['Python', 'Java', 'PHP', 'Java']

bahasa.remove('Java')

print(bahasa)
```

Hasil output `['Python', 'PHP', 'Java']`

##### 2. `list.pop(indeks)`
Fungsi `pop()` akan menghapus nilai sesuai indeks yang dimasukan sebagi parameter, jika tidak ada parameter yang dimasukan fungsi `pop()` akan mengapus nilai yang ada di urutan terakhir.

Contoh:
```python
bahasa = ['Python', 'Java', 'PHP']

# akan menghapus nilai terakhir, yaitu PHP
bahasa.pop() 

# akan menghapus nilai di indeks ke-1, yaitu Java
bahasa.pop(1)

print(bahasa)
```

Hasil output `['Python']`

##### 3. `list.clear()`
Fungsi `clear()` terbilang extreme, karena semua nilai list akan dihapus dengan fungsi ini.

Contoh:
```python
bahasa = ['Python', 'Java', 'PHP']

bahasa.clear()

print(bahasa)
```
Hasil output `[]`

### Menggabungkan List
Kita juga bisa menggabungkan banyak list menjadi satu kesatuan.

Contoh:
```python
bahasa = ['Python', 'Java', 'PHP']
angka = [2, 00.1, 321]
warna = ['Putih', 'Hitam']

semua_list = bahasa + angka + warna

print(semua_list)
```

Hasil output `['Python', 'Java', 'PHP', 2, 0.1, 321, 'Putih', 'Hitam']`

Selain fungsi-fungsi yang sudah saya bahas di atas, masih banyak lagi fungsi _Build-in_ yang disediakan Python untuk menyelesaikan berbagai macam permasalahan pada list.

| Fungsi             | Penggunaan               | Keterangan
|:------------------ |:------------------------ | :--------------------------
| `index()`          | `list.index('Java')`     | Mengetahui posisi indeks
| `count()`          | `list.count('Java')`     | Menghitung nilai yang sama pada suatu list
| `sort()`           | `list.sort()`            | Mengurutkan nilai list
| `copy()`           | `list.copy()`            | Menduplikat nilai list
| `reverse()`        | `list.reverse()`         | Membalikkan posisi nilai pada list
| `len()`            | `len(list)`              | Mengetahui total panjang list
| `min()`            | `min(list)`              | Mengetahui nilai terkecil pada list
| `max()`            | `max(list)`              | Mengetahui nilai terbesar pada list
