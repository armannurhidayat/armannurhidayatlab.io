---
title: "Cara Install Odoo 14.0 Di Windows"
subtitle: ""
date: 2021-10-12T10:16:50+07:00
lastmod: 2021-10-12T10:16:50+07:00
draft: false
author: "Arman"
authorLink: "https://www.linkedin.com/in/armannurhidayat"
description: "Cara install Odoo di windows"

tags: ["Odoo"]
categories: ["Odoo"]

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: "armannurhidayat-blog-cara-install-odoo-14-di-windows.jpg"
featuredImagePreview: "armannurhidayat-blog-cara-install-odoo-14-di-windows.jpg"

toc:
  enable: false
math:
  enable: false
lightgallery: true
license: ""
---

Sistem ERP (Enterprise Resouce Planning) menjadi salah satu solusi bagi perusahaan dengan bisnis yang sudah berkembang semakin besar dan memiliki kompleksitas tinggi. Sistem ERP sendiri merupakan sebuah sistem yang mendukung proses bisnis dengan menyediakan banyak informasi secara realtime dan terintegrasi antar divisi–divisi perusahaan. Dengan adanya ERP, perusahaan dapat dengan mudah memanajemen data atau informasi dalam jumlah yang besar untuk diolah dan ditampilkan sesuai dengan yang diinginkan.

Sampai saat ini sudah banyak sekali sistem ERP yang telah dikembangkan oleh perushaan-perusahaan software seperti Accurate, SAP, dan yang populer saat ini adalah OpenERP atau Odoo (nama baru dari OpenERP).

Odoo merupakan software manajemen bisnis berbasis web yang didistribusikan secara _open source_, didalamnya sudah di sediakan lengkap berbagai program aplikasi bisnis yang meliputi Sales, CRM, Purchase, Inventory, Manufacturing, Accounting, Payroll, E-commerce, dan masih banyak lagi yang lainnya.

![Preview Odoo Community 14.0](preview-odoo.png "Apps Odoo Community 14.0")

Sesuai dengan judul artikel di atas, pada kesempatan kali ini saya akan membahas bagaimana cara install Odoo 14.0 di sistem operasi Windows.

Langkah pertama kamu bisa mendownload file installer Odoo nya di [www.nightly.odoo.com](http://nightly.odoo.com), saya sarankan untuk mendownload versi terakhir biasanya ada di bagian paling bawah.

![Odoo installation](langkah-instalasi-odoo-01.png)

Setelah proses download selesai, kamu bisa double click file installer Odoo yang sudah di download tadi. Kemudian kamu bisa pilih bahasa yang akan digunakan selama proses instalasi di lakukan dan pilih OK.

![Odoo installation](langkah-instalasi-odoo-02.png)

Click Next untuk melanjutkan.

![Odoo installation](langkah-instalasi-odoo-03.png)

Click I Agree, jika kamu menyetujui Agreement yang di tampilkan.

![Odoo installation](langkah-instalasi-odoo-04.png)

Pada halaman ini, pastikan Odoo Server dan PostgresSQL Database dalam posisi tercentang. Dan click Next.

![Odoo installation](langkah-instalasi-odoo-05.png)

Untuk konfigurasi koneksi ke PostgresSQL Database saran saya biarkan default saja. Kemudian click Next.

![Odoo installation](langkah-instalasi-odoo-06.png)

Pilih folder penyimpanan aplikasi Odoo. Lalu click Install untuk memulai proses installasi.

![Odoo installation](langkah-instalasi-odoo-07.png)

Tunggu proses installasi sampai selesai, ini akan membutuhkan waktu cukup lama, karena banyak dependensi Odoo yang akan di install juga.

![Odoo installation](langkah-instalasi-odoo-08.png)

Setelah proses installasi selesai, kamu bisa click Next.

![Odoo installation](langkah-instalasi-odoo-09.png)

Selamat!, sampai pada halaman ini tandanya proses installasi Odoo 14.0 di Windows kamu sudah selesai. Untuk langsung menjalankan Odoo kamu bisa centang di bagian Start Odoo dan click Finish. Atau bisa mengunjungi `http://localhost:8069` pada browser.

![Odoo installation](langkah-instalasi-odoo-10.png)

Untuk membuat database baru di Odoo 14.0 ini berbeda dengan versi Odoo sebelumnya, kita di wajibkan untuk mengisi master password. Master password ini hanya dibuat ketika pertama kali membuat database, untuk membuat database baru nanti kedepannya form input master password diisi dengan master password yang sudah dibuat ketika pertama kali buat database.


{{< admonition note >}}
Jika lupa master password kamu bisa melihat atau menggantinya di `C:\Program Files\Odoo 14.0\server\odoo.conf` pada variabel `admin_passwd`, PATH bisa disesuaikan dengan PATH yang kamu arahkan saat installasi Odoo.
{{< /admonition >}}

![Odoo installation](langkah-instalasi-odoo-11.png)

Penjelasan:
* __Master Password__: password yang digunakan untuk mengelola database.
* __Database Name__: nama database.
* __Email__: username login.
* __Password__: password login.
* __Phone Number__: nomor telepon perusahaan.
* __Language__: bahasa yang digunakan.
* __Country__: negara perusahaan.
* __Demo Data__: data dummy.

Berikut tampilan halaman Odoo ketika pertama kali berhasil login.

![Odoo installation](langkah-instalasi-odoo-12.png)