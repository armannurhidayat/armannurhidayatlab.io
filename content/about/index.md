---
title: "About Me."
date: 2019-08-02T11:04:49+08:00
draft: false

lightgallery: true

math:
  enable: true
---

**Halo, Saya [Arman Nur Hidayat](mailto:armannurhidayat7@gmail.com)** 👋

Saya adalah seorang Odoo Developer berpengalaman yang memiliki semangat dalam menciptakan solusi ERP yang efisien, ramah pengguna, dan disesuaikan. Dengan pengalaman sejak tahun 2019, saya mengkhususkan diri dalam merancang mengimplementasikan, dan mengoptimalkan modul Odoo untuk membantu bisnis menyederhanakan operasional mereka. Dan saya juga ikut berkontribusi untuk membangun modul Odoo di [Odoo Apps](https://apps.odoo.com/apps/modules/browse?search=arman+nur+hidayat).


##### Proyek yang Pernah Saya Kerjakan

* **PT Toffin Indonesia**

Membangun sistem E-Learning yang menyediakan program pengembangan dan pelatihan industri kopi mulai dari kreasi menu, bisnis, hingga keterampilan oleh mentor berpengalaman.

* **PT Teratai Widjaja**

Mengembangkan dan menyesuaikan berbagai modul Odoo untuk memenuhi kebutuhan operasional perusahaan, termasuk modul manajemen inventaris dan pengelolaan produksi.

* **PT Brantas Abipraya**

Mengembangkan modul Work Breakdown Structure (WBS) yang dirancang khusus untuk mendukung proses internal perusahaan, termasuk manajemen proyek dan pengelolaan inventaris.
