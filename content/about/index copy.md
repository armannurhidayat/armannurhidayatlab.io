---
title: "About Me."
date: 2019-08-02T11:04:49+08:00
draft: false

lightgallery: true

math:
  enable: true
---

[Arman Nur Hidayat](mailto:armannurhidayat7@gmail.com), seorang nomaden, kelahiran Lampung lebih tepatnya di kota Metro pada tahun 1997, kecil dan besar bersama kedua orang tua di kota Karawang, tersesat kuliah jurusan Teknik Informatika di Universitas Suryakancana kota Cianjur, dan saat ini saya sedang bekerja di salah satu perusahan di kota Bandung sebagai Odoo Developer.

Tujuan sebenarnya dari dibuatnya blog ini sebagai catatan saya selama belajar pemrograman. Hal baru yang dipelajari akan langsung dicatat disini, agar tidak lupa nantinya. Dan sesekali mungkin juga akan diselipkan catatan tentang kehidupan sehari-hari saya.

Semoga selain menjadi catatan pribadi saya, adanya blog ini bisa bermanfaat. Saya berharap, kalian menikmati membacanya seperti saya menikmati menulisnya.

Terima kasih sudah berkunjung. :wave: